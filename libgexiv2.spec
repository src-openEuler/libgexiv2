Name:           libgexiv2
Version:        0.14.3
Release:        1
Summary:        Gexiv2 is a GObject wrapper around the Exiv2 photo metadata library.
License:        GPLv2+
URL:            https://wiki.gnome.org/Projects/gexiv2
Source0:        https://download.gnome.org/sources/gexiv2/0.14/gexiv2-%{version}.tar.xz

BuildRequires:  gcc-c++ exiv2-devel gobject-introspection-devel meson
BuildRequires:  libtool gtk-doc vala
BuildRequires:  python3-devel python3-gobject-base

%description
gexiv2 is a GObject wrapper around the Exiv2 photo metadata library. It allows for GNOME
applications to easily inspect and update EXIF, IPTC, and XMP metadata in photo and video
files of various formats.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
Files for %{name} development

%package -n     python3-gexiv2
Summary:        Python3 support
Requires:       %{name} = %{version}-%{release}
Requires:       python3-gobject-base

%description -n python3-gexiv2
This package contains support for python3

%package_help

%prep
%autosetup -n gexiv2-%{version} -p1

%build
%meson \
  -Dgtk_doc=true \
  -Dtests=true \
  %{nil}
%meson_build

%install
%meson_install
%delete_la

%check
%meson_test

#Install and uninstall scripts
%pre

%preun

%ldconfig_scriptlets

%files
%doc AUTHORS THANKS README.md
%license COPYING
%{_libdir}/libgexiv2.so.2*
%{_libdir}/girepository-1.0/GExiv2-0.10.typelib

%files devel
%{_includedir}/gexiv2/
%{_libdir}/libgexiv2.so
%{_libdir}/pkgconfig/gexiv2.pc
%{_datadir}/gir-1.0/GExiv2-0.10.gir
%{_datadir}/vala/vapi/gexiv2.deps

%files -n python3-gexiv2
%{_prefix}/lib/python%{python3_version}/site-packages/gi/overrides/__pycache__/GExiv2*
%{_prefix}/lib/python%{python3_version}/site-packages/gi/overrides/GExiv2.py

%files help
%{_datadir}/gtk-doc/html/gexiv2/
%{_datadir}/vala/vapi/gexiv2.vapi

%changelog
* Mon Jul 29 2024 dillon chen <dillon.chen@gmail.com> - 0.14.3-1
- Update to 0.14.3

* Fri Sep  8 2023 dillon chen <dillon.chen@gmail.com> - 0.14.2-1
- Update to 0.14.2

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.14.0-1
- Update to 0.14.0

* Tue Feb 2 2021 liudabo <liudabo1@huawei.com> - 0.12.1-1
- upgrade version to 0.12.1

* Fri Oct 30 2020 wuchaochao <wuchaochao4@huawei.com> - 0.10.8-6
- Type:bufix
- CVE:NA
- SUG:NA
- DESC:remove python2

* Wed Jul 18 2018 openEuler Buildteam <buildteam@openeuler.org> - 0.10.8-5
- Package init
